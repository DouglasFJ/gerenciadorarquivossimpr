package br.gov.caixa.gerenciadorarquivossimpr;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.plaf.FileChooserUI;

import br.gov.caixa.gerenciadorarquivossimpr.models.Remessa;
import br.gov.caixa.gerenciadorarquivossimpr.services.FileServices;

public class Main {

	static int cont = 1;
	
	public static void main(String[] args) {
		
		try {
			String caminhoArquivo = JOptionPane.showInputDialog("Digite o caminho do arquivo:");
			
			byte[] bytesArq = Files.readAllBytes(Paths.get(caminhoArquivo));
			
			List<Remessa> remessas = FileServices.fileToRemessas(bytesArq);
			
			remessas.forEach((remessa)->{
				remessa = FileServices.atualizarRemessa(remessa, cont);
				remessa.atualizarNumSeq();
				cont();
			});
			
			FileServices.remessaToFile(caminhoArquivo, remessas);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private static void cont() {
		cont++;
	}

}
