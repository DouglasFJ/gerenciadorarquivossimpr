package br.gov.caixa.gerenciadorarquivossimpr.services;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import br.gov.caixa.gerenciadorarquivossimpr.models.HeaderRemessa;
import br.gov.caixa.gerenciadorarquivossimpr.models.Remessa;
import br.gov.caixa.gerenciadorarquivossimpr.models.TraillerRemessa;
import br.gov.caixa.gerenciadorarquivossimpr.models.TransacaoRemessa;

public class FileServices {
	
	public static List<Remessa> fileToRemessas(byte[] file) {
		String sArq = new String(file);
		
		String[] sLines = sArq.split("\n");
		if (sLines.length < 3) {
			sLines = sArq.split("\r");
		}
		
		List<String> linhas = Arrays.asList(sLines);
		
		List<Object> objLinhas = linhas.stream()
				.map((linha)->{
					
					if(linha.startsWith("0")) {
						HeaderRemessa header = new HeaderRemessa();
						
						header.setIdRegistro(linha.substring(0, 1));
						header.setCoPortador(linha.substring(1, 4));
						header.setNoPortador(linha.substring(4, 44));
						header.setDtMovimento(linha.substring(44,52));
						header.setIdTransRemetente(linha.substring(52, 55));
						header.setIdTransDestinatario(linha.substring(55,58));
						header.setIdTransTipo(linha.substring(58,61));
						header.setNuSeqRemessa(linha.substring(61,67));
						header.setQtRegistrosRmsa(linha.substring(67,71));
						header.setQtTitulosRmsa(linha.substring(71, 75));
						header.setQtIndicacoesRmsa(linha.substring(75, 79));
						header.setQtOriginaisRmsa(linha.substring(79, 83));
						header.setIdAgenciaCra(linha.substring(83, 89));
						header.setVersaoLayout(linha.substring(89, 92));
						header.setCoMunPracaPagamento(linha.substring(92, 99));
						header.setComplRegistro(linha.substring(99, 596));
						header.setNuSeqRegistroArq(linha.substring(596));
						
						return header;
					}else if (linha.startsWith("1")) {
						TransacaoRemessa tr = new TransacaoRemessa();
						
						tr.setIdRegistro(linha.substring(0,1));
						tr.setCoPortador(linha.substring(1,4));
						tr.setAgenciaCoCedente(linha.substring(4,19));
						tr.setNoCedente(linha.substring(19,64));
						tr.setNoSacador(linha.substring(64,109));
						tr.setDocSacador(linha.substring(109,123));
						tr.setEnderecoSacador(linha.substring(123,168));
						tr.setCepSacador(linha.substring(168,176));
						tr.setCidadeSacador(linha.substring(176,196));
						tr.setUfSacador(linha.substring(196,198));
						tr.setNossoNum(linha.substring(198,213));
						tr.setEspecieTitulo(linha.substring(213,216));
						tr.setNumTitulo(linha.substring(216,227)); 
						tr.setDtEmissaoTitulo(linha.substring(227,235));
						tr.setDtVencimentoTitulo(linha.substring(235,243));
						tr.setTipoMoeda(linha.substring(243,246));
						tr.setVrTitulo(linha.substring(246,260));
						tr.setSaldoTitulo(linha.substring(260,274));
						tr.setPracaProtesto(linha.substring(274,294));
						tr.setTipoEndosso(linha.substring(294,295));
						tr.setInfoAceite(linha.substring(295,296));
						tr.setNuCtrlDevedores(linha.substring(296,297));
						tr.setNoDevedor(linha.substring(297,342));
						tr.setTipoIdDevedor(linha.substring(342,345));
						tr.setNuIdDevedor(linha.substring(345,359));
						tr.setDocDevedor(linha.substring(359,370));
						tr.setEnderecoDevedor(linha.substring(370,415));
						tr.setCepDevedor(linha.substring(415,423));
						tr.setCidadeDevedor(linha.substring(423,443));
						tr.setUfDevedor(linha.substring(443,445));
						tr.setCoCartorio(linha.substring(445,447));
						tr.setNuProtocoloCartorio(linha.substring(447,457));
						tr.setTipoOcorrencia(linha.substring(457,458));
						tr.setDtProtocolo(linha.substring(458,466));
						tr.setVrCustasCartorio(linha.substring(466,476));
						tr.setDeclaracaoPortador(linha.substring(476,477));
						tr.setDtOcorrencia(linha.substring(477,485));
						tr.setCoIrregularidade(linha.substring(485,487));
						tr.setBairroDevedor(linha.substring(487,507));
						tr.setVrCustasCartorioDistribuidor(linha.substring(507,517));
						tr.setRegistroDistribuicao(linha.substring(517,523));
						tr.setVrGravacaoEletronica(linha.substring(523,533));
						tr.setNuOperacaoBanco(linha.substring(533,538));
						tr.setNuContratoBanco(linha.substring(538,553));
						tr.setNuParcelaContrato(linha.substring(553,556));
						tr.setTipoLetraCambio(linha.substring(556,557));
						tr.setCompCodigoIrregularidade(linha.substring(557,565));
						tr.setProtestoMotivoFalencia(linha.substring(565,566));
						tr.setInstrumentoProtesto(linha.substring(566,567));
						tr.setVrDemaisDespesas(linha.substring(567,577));
						tr.setComplementoRegistro(linha.substring(577,596));
						tr.setNuSeqRegistroArquivo(linha.substring(596,600));
						
						return tr;
					} else if(linha.startsWith("9")) {
						TraillerRemessa trailler = new TraillerRemessa();
						
						trailler.setIdRegistro(linha.substring(0,1));
						trailler.setCoPortador(linha.substring(1,4));
						trailler.setNoPortador(linha.substring(4,44));
						trailler.setDtMovimento(linha.substring(44,52));
						trailler.setSomaSegurancaQtRemessa(linha.substring(52,57));
						trailler.setSomaSegurancaVrRemessa(linha.substring(57,75));
						trailler.setCompRegistro(linha.substring(75,596));
						trailler.setNuSeqRegistroArq(linha.substring(596,600));
						
						return trailler;
					}else {
						return linha;
					}
				})
				.collect(Collectors.toList());
		
		List<Remessa> remessas = new ArrayList<>();
		Remessa remessa = new Remessa();
		List<TransacaoRemessa> transacoes = new ArrayList<>();
		
		objLinhas.forEach((linha)->{
			if(linha.getClass() == HeaderRemessa.class) {
				HeaderRemessa linhaHeader = (HeaderRemessa) linha;
				remessa.setHeader(linhaHeader);
				
			}else if (linha.getClass() == TransacaoRemessa.class) {
				TransacaoRemessa linhaTransacao = (TransacaoRemessa) linha;
				transacoes.add(linhaTransacao);
				
			}else if (linha.getClass() == TraillerRemessa.class) {
				TraillerRemessa linhaTrailler = (TraillerRemessa) linha;
				remessa.setTrailler(linhaTrailler);
				remessa.setTransacoes(transacoes);
				
				try {
					remessas.add(remessa.clone());
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
				
				transacoes.clear();
			}
			
		});
		
		return remessas;
	}
	
	public static Remessa atualizarRemessa(Remessa remessa, int NSA) {
		
		LocalDate dataAtual = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
		DecimalFormat f6 = new DecimalFormat("000000");
		DecimalFormat f4 = new DecimalFormat("0000");
		DecimalFormat f5 = new DecimalFormat("00000");
		DecimalFormat f18 = new DecimalFormat("000000000000000000");
		
		//--------HEADER-----------
		HeaderRemessa header = remessa.getHeader();
		List<TransacaoRemessa> transacoes = remessa.getTransacoes();
		
		
		//Atualizando data
		header.setDtMovimento(dataAtual.format(formatter));
		
		//NSA - Remessa
		header.setNuSeqRemessa(f6.format(NSA));
		
		// qunt registros
		header.setQtRegistrosRmsa(f4.format(transacoes.size()));
		
		//qunt titulo
		header.setQtTitulosRmsa(f4.format(transacoes.size()));
		
		//Indicações e Originais
		int qtIndic = 0;
		int qtOrig = 0;
		for (int i=0; i<transacoes.size(); i++) {
			TransacaoRemessa tr = transacoes.get(i);
			String especie = tr.getEspecieTitulo();
			if (especie.equals("DMI") || especie.equals("DRI") || especie.equals("CBI"))
				qtIndic++;
			else qtOrig++;
			
		}
		header.setQtIndicacoesRmsa(f4.format(qtIndic));
		header.setQtOriginaisRmsa(f4.format(qtOrig));
		
		//versao
		header.setVersaoLayout("043");
		
		remessa.setHeader(header);
		
		//--------TRAILLER-----------
		TraillerRemessa trailler = remessa.getTrailler();
		
		trailler.setDtMovimento(dataAtual.format(formatter));
		//------------
		int somaQtRemessa = Integer.parseInt(header.getQtIndicacoesRmsa()) + 
				Integer.parseInt(header.getQtOriginaisRmsa()) + 
				Integer.parseInt(header.getQtRegistrosRmsa()) +
				Integer.parseInt(header.getQtTitulosRmsa());
		
		trailler.setSomaSegurancaQtRemessa(f5.format(somaQtRemessa));
		//-------------
		
		//TODO
		double somaSaldo = 0;
		for (int i=0; i<transacoes.size();i++) {
			String sSaldo = transacoes.get(i).getVrTitulo();	
			somaSaldo += (Double.parseDouble(sSaldo)/100);
			
		}
		
		trailler.setSomaSegurancaVrRemessa(f18.format(somaSaldo*100));
		
		remessa.setTrailler(trailler);
		
		return remessa;
	}

	public static boolean remessaToFile(String caminhoArquivo, List<Remessa> remessas) throws Exception {
		
		String sFile = new String();
		
		File file = new File(caminhoArquivo+"_COPIA");
		int cont = 0;
		while(file.exists()) {
			file = new File(caminhoArquivo+"_COPIA_"+cont);
			cont++;
		}
		
		FileWriter fr = new FileWriter(file);
		
		for(int i=0; i<remessas.size();i++) {
			Remessa remessa = remessas.get(i);
			HeaderRemessa header = remessa.getHeader();
			List<TransacaoRemessa> transacoes = remessa.getTransacoes();
			TraillerRemessa trailler = remessa.getTrailler();
			
			//Header
			sFile = sFile.concat(header.getIdRegistro());
			sFile = sFile.concat(header.getCoPortador());
			sFile = sFile.concat(header.getNoPortador());
			sFile = sFile.concat(header.getDtMovimento());
			sFile = sFile.concat(header.getIdTransRemetente());
			sFile = sFile.concat(header.getIdTransDestinatario());
			sFile = sFile.concat(header.getIdTransTipo());
			sFile = sFile.concat(header.getNuSeqRemessa());
			sFile = sFile.concat(header.getQtRegistrosRmsa());
			sFile = sFile.concat(header.getQtTitulosRmsa());
			sFile = sFile.concat(header.getQtIndicacoesRmsa());
			sFile = sFile.concat(header.getQtOriginaisRmsa());
			sFile = sFile.concat(header.getIdAgenciaCra());
			sFile = sFile.concat(header.getVersaoLayout());
			sFile = sFile.concat(header.getCoMunPracaPagamento());
			sFile = sFile.concat(header.getComplRegistro());
			sFile = sFile.concat(header.getNuSeqRegistroArq());
			sFile = sFile.concat("\n\u2026\n");
			
			//Transacoes
			for(int in=0; in<transacoes.size(); in++) {
				TransacaoRemessa transacao = transacoes.get(in);
				
				sFile = sFile.concat( transacao.getIdRegistro());
				sFile = sFile.concat( transacao.getCoPortador());
				sFile = sFile.concat( transacao.getAgenciaCoCedente());
				sFile = sFile.concat( transacao.getNoCedente());
				sFile = sFile.concat( transacao.getNoSacador());
				sFile = sFile.concat( transacao.getDocSacador());
				sFile = sFile.concat( transacao.getEnderecoSacador());
				sFile = sFile.concat( transacao.getCepSacador());
				sFile = sFile.concat( transacao.getCidadeSacador());
				sFile = sFile.concat( transacao.getUfSacador());
				sFile = sFile.concat( transacao.getNossoNum());
				sFile = sFile.concat( transacao.getEspecieTitulo());
				sFile = sFile.concat( transacao.getNumTitulo());
				sFile = sFile.concat( transacao.getDtEmissaoTitulo());
				sFile = sFile.concat( transacao.getDtVencimentoTitulo());
				sFile = sFile.concat( transacao.getTipoMoeda());
				sFile = sFile.concat( transacao.getVrTitulo());
				sFile = sFile.concat( transacao.getSaldoTitulo());
				sFile = sFile.concat( transacao.getPracaProtesto());
				sFile = sFile.concat( transacao.getTipoEndosso());
				sFile = sFile.concat( transacao.getInfoAceite());
				sFile = sFile.concat( transacao.getNuCtrlDevedores());
				sFile = sFile.concat( transacao.getNoDevedor());
				sFile = sFile.concat( transacao.getTipoIdDevedor());
				sFile = sFile.concat( transacao.getNuIdDevedor());
				sFile = sFile.concat( transacao.getDocDevedor());
				sFile = sFile.concat( transacao.getEnderecoDevedor());
				sFile = sFile.concat( transacao.getCepDevedor());
				sFile = sFile.concat( transacao.getCidadeDevedor());
				sFile = sFile.concat( transacao.getUfDevedor());
				sFile = sFile.concat( transacao.getCoCartorio());
				sFile = sFile.concat( transacao.getNuProtocoloCartorio());
				sFile = sFile.concat( transacao.getTipoOcorrencia());
				sFile = sFile.concat( transacao.getDtProtocolo());
				sFile = sFile.concat( transacao.getVrCustasCartorio());
				sFile = sFile.concat( transacao.getDeclaracaoPortador());
				sFile = sFile.concat( transacao.getDtOcorrencia());
				sFile = sFile.concat( transacao.getCoIrregularidade());
				sFile = sFile.concat( transacao.getBairroDevedor());
				sFile = sFile.concat( transacao.getVrCustasCartorioDistribuidor());
				sFile = sFile.concat( transacao.getRegistroDistribuicao());
				sFile = sFile.concat( transacao.getVrGravacaoEletronica());
				sFile = sFile.concat( transacao.getNuOperacaoBanco());
				sFile = sFile.concat( transacao.getNuContratoBanco());
				sFile = sFile.concat( transacao.getNuParcelaContrato());
				sFile = sFile.concat( transacao.getTipoLetraCambio());
				sFile = sFile.concat( transacao.getCompCodigoIrregularidade());
				sFile = sFile.concat( transacao.getProtestoMotivoFalencia());
				sFile = sFile.concat( transacao.getInstrumentoProtesto());
				sFile = sFile.concat( transacao.getVrDemaisDespesas());
				sFile = sFile.concat( transacao.getComplementoRegistro());
				sFile = sFile.concat( transacao.getNuSeqRegistroArquivo());
				sFile = sFile.concat("\n\u2026\n");

			}
			
			//Trailler
			
			sFile = sFile.concat(trailler.getIdRegistro());
			sFile = sFile.concat(trailler.getCoPortador());
			sFile = sFile.concat(trailler.getNoPortador());
			sFile = sFile.concat(trailler.getDtMovimento());
			sFile = sFile.concat(trailler.getSomaSegurancaQtRemessa());
			sFile = sFile.concat(trailler.getSomaSegurancaVrRemessa());
			sFile = sFile.concat(trailler.getCompRegistro());
			sFile = sFile.concat(trailler.getNuSeqRegistroArq());
			sFile = sFile.concat("\n\u2026\n");

		}
		
		fr.write(sFile);
		fr.close();
		
		return false;
	}
}
