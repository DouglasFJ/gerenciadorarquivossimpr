package br.gov.caixa.gerenciadorarquivossimpr.models;

public class HeaderRemessa {
	private String idRegistro;
    private String coPortador;
    private String noPortador;
    private String dtMovimento;
    private String idTransRemetente;
    private String idTransDestinatario;
    private String idTransTipo;
    private String nuSeqRemessa;
    private String qtRegistrosRmsa;
    private String qtTitulosRmsa;
    private String qtIndicacoesRmsa;
    private String qtOriginaisRmsa;
    private String idAgenciaCra;
    private String versaoLayout;
    private String coMunPracaPagamento;
    private String complRegistro;
    private String nuSeqRegistroArq;
	public String getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(String idRegistro) {
		this.idRegistro = idRegistro;
	}
	public String getCoPortador() {
		return coPortador;
	}
	public void setCoPortador(String coPortador) {
		this.coPortador = coPortador;
	}
	public String getNoPortador() {
		return noPortador;
	}
	public void setNoPortador(String noPortador) {
		this.noPortador = noPortador;
	}
	public String getDtMovimento() {
		return dtMovimento;
	}
	public void setDtMovimento(String dtMovimento) {
		this.dtMovimento = dtMovimento;
	}
	public String getIdTransRemetente() {
		return idTransRemetente;
	}
	public void setIdTransRemetente(String idTransRemetente) {
		this.idTransRemetente = idTransRemetente;
	}
	public String getIdTransDestinatario() {
		return idTransDestinatario;
	}
	public void setIdTransDestinatario(String idTransDestinatario) {
		this.idTransDestinatario = idTransDestinatario;
	}
	public String getIdTransTipo() {
		return idTransTipo;
	}
	public void setIdTransTipo(String idTransTipo) {
		this.idTransTipo = idTransTipo;
	}
	public String getNuSeqRemessa() {
		return nuSeqRemessa;
	}
	public void setNuSeqRemessa(String nuSeqRemessa) {
		this.nuSeqRemessa = nuSeqRemessa;
	}
	public String getQtRegistrosRmsa() {
		return qtRegistrosRmsa;
	}
	public void setQtRegistrosRmsa(String qtRegistrosRmsa) {
		this.qtRegistrosRmsa = qtRegistrosRmsa;
	}
	public String getQtTitulosRmsa() {
		return qtTitulosRmsa;
	}
	public void setQtTitulosRmsa(String qtTitulosRmsa) {
		this.qtTitulosRmsa = qtTitulosRmsa;
	}
	public String getQtIndicacoesRmsa() {
		return qtIndicacoesRmsa;
	}
	public void setQtIndicacoesRmsa(String qtIndicacoesRmsa) {
		this.qtIndicacoesRmsa = qtIndicacoesRmsa;
	}
	public String getQtOriginaisRmsa() {
		return qtOriginaisRmsa;
	}
	public void setQtOriginaisRmsa(String qtOriginaisRmsa) {
		this.qtOriginaisRmsa = qtOriginaisRmsa;
	}
	public String getIdAgenciaCra() {
		return idAgenciaCra;
	}
	public void setIdAgenciaCra(String idAgenciaCra) {
		this.idAgenciaCra = idAgenciaCra;
	}
	public String getVersaoLayout() {
		return versaoLayout;
	}
	public void setVersaoLayout(String versaoLayout) {
		this.versaoLayout = versaoLayout;
	}
	public String getCoMunPracaPagamento() {
		return coMunPracaPagamento;
	}
	public void setCoMunPracaPagamento(String coMunPracaPagamento) {
		this.coMunPracaPagamento = coMunPracaPagamento;
	}
	public String getComplRegistro() {
		return complRegistro;
	}
	public void setComplRegistro(String complRegistro) {
		this.complRegistro = complRegistro;
	}
	public String getNuSeqRegistroArq() {
		return nuSeqRegistroArq;
	}
	public void setNuSeqRegistroArq(String nuSeqRegistroArq) {
		this.nuSeqRegistroArq = nuSeqRegistroArq;
	}
	@Override
	public String toString() {
		return "HeaderRemessa [idRegistro=" + idRegistro + ",\n coPortador=" + coPortador + ",\n noPortador=" + noPortador
				+ ",\n dtMovimento=" + dtMovimento + ",\n idTransRemetente=" + idTransRemetente + ",\n idTransDestinatario="
				+ idTransDestinatario + ",\n idTransTipo=" + idTransTipo + ",\n nuSeqRemessa=" + nuSeqRemessa
				+ ",\n qtRegistrosRmsa=" + qtRegistrosRmsa + ",\n qtTitulosRmsa=" + qtTitulosRmsa + ",\n qtIndicacoesRmsa="
				+ qtIndicacoesRmsa + ",\n qtOriginaisRmsa=" + qtOriginaisRmsa + ",\n idAgenciaCra=" + idAgenciaCra
				+ ",\n versaoLayout=" + versaoLayout + ",\n coMunPracaPagamento=" + coMunPracaPagamento + ",\n complRegistro="
				+ complRegistro + ",\n nuSeqRegistroArq=" + nuSeqRegistroArq + "]";
	}
    
    
}
