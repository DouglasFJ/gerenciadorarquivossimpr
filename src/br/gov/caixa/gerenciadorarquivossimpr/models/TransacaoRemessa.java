package br.gov.caixa.gerenciadorarquivossimpr.models;

public class TransacaoRemessa {

	private String idRegistro;
    private String coPortador;
    private String agenciaCoCedente;
    private String noCedente;
    private String noSacador;
    private String docSacador;
    private String enderecoSacador;
    private String cepSacador;
    private String cidadeSacador;
    private String ufSacador;
    private String nossoNum;
    private String especieTitulo;
    private String numTitulo;
    private String dtEmissaoTitulo;
    private String dtVencimentoTitulo;
    private String tipoMoeda;
    private String vrTitulo;
    private String saldoTitulo;
    private String pracaProtesto;
    private String tipoEndosso;
    private String infoAceite;
    private String nuCtrlDevedores;
    private String noDevedor;
    private String tipoIdDevedor;
    private String nuIdDevedor;
    private String docDevedor;
    private String enderecoDevedor;
    private String cepDevedor;
    private String cidadeDevedor;
    private String ufDevedor;
    private String coCartorio;
    private String nuProtocoloCartorio;
    private String tipoOcorrencia;
    private String dtProtocolo;
    private String vrCustasCartorio;
    private String declaracaoPortador;
    private String dtOcorrencia;
    private String coIrregularidade;
    private String bairroDevedor;
    private String vrCustasCartorioDistribuidor;
    private String registroDistribuicao;
    private String vrGravacaoEletronica;
    private String nuOperacaoBanco;
    private String nuContratoBanco;
    private String nuParcelaContrato;
    private String tipoLetraCambio;
    private String compCodigoIrregularidade;
    private String protestoMotivoFalencia;
    private String instrumentoProtesto;
    private String vrDemaisDespesas;
    private String complementoRegistro;
    private String nuSeqRegistroArquivo;
	public String getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(String idRegistro) {
		this.idRegistro = idRegistro;
	}
	public String getCoPortador() {
		return coPortador;
	}
	public void setCoPortador(String coPortador) {
		this.coPortador = coPortador;
	}
	public String getAgenciaCoCedente() {
		return agenciaCoCedente;
	}
	public void setAgenciaCoCedente(String agenciaCoCedente) {
		this.agenciaCoCedente = agenciaCoCedente;
	}
	public String getNoCedente() {
		return noCedente;
	}
	public void setNoCedente(String noCedente) {
		this.noCedente = noCedente;
	}
	public String getNoSacador() {
		return noSacador;
	}
	public void setNoSacador(String noSacador) {
		this.noSacador = noSacador;
	}
	public String getDocSacador() {
		return docSacador;
	}
	public void setDocSacador(String docSacador) {
		this.docSacador = docSacador;
	}
	public String getEnderecoSacador() {
		return enderecoSacador;
	}
	public void setEnderecoSacador(String enderecoSacador) {
		this.enderecoSacador = enderecoSacador;
	}
	public String getCepSacador() {
		return cepSacador;
	}
	public void setCepSacador(String cepSacador) {
		this.cepSacador = cepSacador;
	}
	public String getCidadeSacador() {
		return cidadeSacador;
	}
	public void setCidadeSacador(String cidadeSacador) {
		this.cidadeSacador = cidadeSacador;
	}
	public String getUfSacador() {
		return ufSacador;
	}
	public void setUfSacador(String ufSacador) {
		this.ufSacador = ufSacador;
	}
	public String getNossoNum() {
		return nossoNum;
	}
	public void setNossoNum(String nossoNum) {
		this.nossoNum = nossoNum;
	}
	public String getEspecieTitulo() {
		return especieTitulo;
	}
	public void setEspecieTitulo(String especieTitulo) {
		this.especieTitulo = especieTitulo;
	}
	public String getNumTitulo() {
		return numTitulo;
	}
	public void setNumTitulo(String numTitulo) {
		this.numTitulo = numTitulo;
	}
	public String getDtEmissaoTitulo() {
		return dtEmissaoTitulo;
	}
	public void setDtEmissaoTitulo(String dtEmissaoTitulo) {
		this.dtEmissaoTitulo = dtEmissaoTitulo;
	}
	public String getDtVencimentoTitulo() {
		return dtVencimentoTitulo;
	}
	public void setDtVencimentoTitulo(String dtVencimentoTitulo) {
		this.dtVencimentoTitulo = dtVencimentoTitulo;
	}
	public String getTipoMoeda() {
		return tipoMoeda;
	}
	public void setTipoMoeda(String tipoMoeda) {
		this.tipoMoeda = tipoMoeda;
	}
	public String getVrTitulo() {
		return vrTitulo;
	}
	public void setVrTitulo(String vrTitulo) {
		this.vrTitulo = vrTitulo;
	}
	public String getSaldoTitulo() {
		return saldoTitulo;
	}
	public void setSaldoTitulo(String saldoTitulo) {
		this.saldoTitulo = saldoTitulo;
	}
	public String getPracaProtesto() {
		return pracaProtesto;
	}
	public void setPracaProtesto(String pracaProtesto) {
		this.pracaProtesto = pracaProtesto;
	}
	public String getTipoEndosso() {
		return tipoEndosso;
	}
	public void setTipoEndosso(String tipoEndosso) {
		this.tipoEndosso = tipoEndosso;
	}
	public String getInfoAceite() {
		return infoAceite;
	}
	public void setInfoAceite(String infoAceite) {
		this.infoAceite = infoAceite;
	}
	public String getNuCtrlDevedores() {
		return nuCtrlDevedores;
	}
	public void setNuCtrlDevedores(String nuCtrlDevedores) {
		this.nuCtrlDevedores = nuCtrlDevedores;
	}
	public String getNoDevedor() {
		return noDevedor;
	}
	public void setNoDevedor(String noDevedor) {
		this.noDevedor = noDevedor;
	}
	public String getTipoIdDevedor() {
		return tipoIdDevedor;
	}
	public void setTipoIdDevedor(String tipoIdDevedor) {
		this.tipoIdDevedor = tipoIdDevedor;
	}
	public String getNuIdDevedor() {
		return nuIdDevedor;
	}
	public void setNuIdDevedor(String nuIdDevedor) {
		this.nuIdDevedor = nuIdDevedor;
	}
	public String getDocDevedor() {
		return docDevedor;
	}
	public void setDocDevedor(String docDevedor) {
		this.docDevedor = docDevedor;
	}
	public String getEnderecoDevedor() {
		return enderecoDevedor;
	}
	public void setEnderecoDevedor(String enderecoDevedor) {
		this.enderecoDevedor = enderecoDevedor;
	}
	public String getCepDevedor() {
		return cepDevedor;
	}
	public void setCepDevedor(String cepDevedor) {
		this.cepDevedor = cepDevedor;
	}
	public String getCidadeDevedor() {
		return cidadeDevedor;
	}
	public void setCidadeDevedor(String cidadeDevedor) {
		this.cidadeDevedor = cidadeDevedor;
	}
	public String getUfDevedor() {
		return ufDevedor;
	}
	public void setUfDevedor(String ufDevedor) {
		this.ufDevedor = ufDevedor;
	}
	public String getCoCartorio() {
		return coCartorio;
	}
	public void setCoCartorio(String coCartorio) {
		this.coCartorio = coCartorio;
	}
	public String getNuProtocoloCartorio() {
		return nuProtocoloCartorio;
	}
	public void setNuProtocoloCartorio(String nuProtocoloCartorio) {
		this.nuProtocoloCartorio = nuProtocoloCartorio;
	}
	public String getTipoOcorrencia() {
		return tipoOcorrencia;
	}
	public void setTipoOcorrencia(String tipoOcorrencia) {
		this.tipoOcorrencia = tipoOcorrencia;
	}
	public String getDtProtocolo() {
		return dtProtocolo;
	}
	public void setDtProtocolo(String dtProtocolo) {
		this.dtProtocolo = dtProtocolo;
	}
	public String getVrCustasCartorio() {
		return vrCustasCartorio;
	}
	public void setVrCustasCartorio(String vrCustasCartorio) {
		this.vrCustasCartorio = vrCustasCartorio;
	}
	public String getDeclaracaoPortador() {
		return declaracaoPortador;
	}
	public void setDeclaracaoPortador(String declaracaoPortador) {
		this.declaracaoPortador = declaracaoPortador;
	}
	public String getDtOcorrencia() {
		return dtOcorrencia;
	}
	public void setDtOcorrencia(String dtOcorrencia) {
		this.dtOcorrencia = dtOcorrencia;
	}
	public String getCoIrregularidade() {
		return coIrregularidade;
	}
	public void setCoIrregularidade(String coIrregularidade) {
		this.coIrregularidade = coIrregularidade;
	}
	public String getBairroDevedor() {
		return bairroDevedor;
	}
	public void setBairroDevedor(String bairroDevedor) {
		this.bairroDevedor = bairroDevedor;
	}
	public String getVrCustasCartorioDistribuidor() {
		return vrCustasCartorioDistribuidor;
	}
	public void setVrCustasCartorioDistribuidor(String vrCustasCartorioDistribuidor) {
		this.vrCustasCartorioDistribuidor = vrCustasCartorioDistribuidor;
	}
	public String getRegistroDistribuicao() {
		return registroDistribuicao;
	}
	public void setRegistroDistribuicao(String registroDistribuicao) {
		this.registroDistribuicao = registroDistribuicao;
	}
	public String getVrGravacaoEletronica() {
		return vrGravacaoEletronica;
	}
	public void setVrGravacaoEletronica(String vrGravacaoEletronica) {
		this.vrGravacaoEletronica = vrGravacaoEletronica;
	}
	public String getNuOperacaoBanco() {
		return nuOperacaoBanco;
	}
	public void setNuOperacaoBanco(String nuOperacaoBanco) {
		this.nuOperacaoBanco = nuOperacaoBanco;
	}
	public String getNuContratoBanco() {
		return nuContratoBanco;
	}
	public void setNuContratoBanco(String nuContratoBanco) {
		this.nuContratoBanco = nuContratoBanco;
	}
	public String getNuParcelaContrato() {
		return nuParcelaContrato;
	}
	public void setNuParcelaContrato(String nuParcelaContrato) {
		this.nuParcelaContrato = nuParcelaContrato;
	}
	public String getTipoLetraCambio() {
		return tipoLetraCambio;
	}
	public void setTipoLetraCambio(String tipoLetraCambio) {
		this.tipoLetraCambio = tipoLetraCambio;
	}
	public String getCompCodigoIrregularidade() {
		return compCodigoIrregularidade;
	}
	public void setCompCodigoIrregularidade(String compCodigoIrregularidade) {
		this.compCodigoIrregularidade = compCodigoIrregularidade;
	}
	public String getProtestoMotivoFalencia() {
		return protestoMotivoFalencia;
	}
	public void setProtestoMotivoFalencia(String protestoMotivoFalencia) {
		this.protestoMotivoFalencia = protestoMotivoFalencia;
	}
	public String getInstrumentoProtesto() {
		return instrumentoProtesto;
	}
	public void setInstrumentoProtesto(String instrumentoProtesto) {
		this.instrumentoProtesto = instrumentoProtesto;
	}
	public String getVrDemaisDespesas() {
		return vrDemaisDespesas;
	}
	public void setVrDemaisDespesas(String vrDemaisDespesas) {
		this.vrDemaisDespesas = vrDemaisDespesas;
	}
	public String getComplementoRegistro() {
		return complementoRegistro;
	}
	public void setComplementoRegistro(String complementoRegistro) {
		this.complementoRegistro = complementoRegistro;
	}
	public String getNuSeqRegistroArquivo() {
		return nuSeqRegistroArquivo;
	}
	public void setNuSeqRegistroArquivo(String nuSeqRegistroArquivo) {
		this.nuSeqRegistroArquivo = nuSeqRegistroArquivo;
	}
	@Override
	public String toString() {
		return "TransacaoRemessa [idRegistro=" + idRegistro + ",\n coPortador=" + coPortador + ",\n agenciaCoCedente="
				+ agenciaCoCedente + ",\n noCedente=" + noCedente + ",\n noSacador=" + noSacador + ",\n docSacador="
				+ docSacador + ",\n enderecoSacador=" + enderecoSacador + ",\n cepSacador=" + cepSacador
				+ ",\n cidadeSacador=" + cidadeSacador + ",\n ufSacador=" + ufSacador + ",\n nossoNum=" + nossoNum
				+ ",\n especieTitulo=" + especieTitulo + ",\n numTitulo=" + numTitulo + ",\n dtEmissaoTitulo="
				+ dtEmissaoTitulo + ",\n dtVencimentoTitulo=" + dtVencimentoTitulo + ",\n tipoMoeda=" + tipoMoeda
				+ ",\n vrTitulo=" + vrTitulo + ",\n saldoTitulo=" + saldoTitulo + ",\n pracaProtesto=" + pracaProtesto
				+ ",\n tipoEndosso=" + tipoEndosso + ",\n infoAceite=" + infoAceite + ",\n nuCtrlDevedores=" + nuCtrlDevedores
				+ ",\n noDevedor=" + noDevedor + ",\n tipoIdDevedor=" + tipoIdDevedor + ",\n nuIdDevedor=" + nuIdDevedor
				+ ",\n docDevedor=" + docDevedor + ",\n enderecoDevedor=" + enderecoDevedor + ",\n cepDevedor=" + cepDevedor
				+ ",\n cidadeDevedor=" + cidadeDevedor + ",\n ufDevedor=" + ufDevedor + ",\n coCartorio=" + coCartorio
				+ ",\n nuProtocoloCartorio=" + nuProtocoloCartorio + ",\n tipoOcorrencia=" + tipoOcorrencia
				+ ",\n dtProtocolo=" + dtProtocolo + ",\n vrCustasCartorio=" + vrCustasCartorio + ",\n declaracaoPortador="
				+ declaracaoPortador + ",\n dtOcorrencia=" + dtOcorrencia + ",\n coIrregularidade=" + coIrregularidade
				+ ",\n bairroDevedor=" + bairroDevedor + ",\n vrCustasCartorioDistribuidor=" + vrCustasCartorioDistribuidor
				+ ",\n registroDistribuicao=" + registroDistribuicao + ",\n vrGravacaoEletronica=" + vrGravacaoEletronica
				+ ",\n nuOperacaoBanco=" + nuOperacaoBanco + ",\n nuContratoBanco=" + nuContratoBanco
				+ ",\n nuParcelaContrato=" + nuParcelaContrato + ",\n tipoLetraCambio=" + tipoLetraCambio
				+ ",\n compCodigoIrregularidade=" + compCodigoIrregularidade + ",\n protestoMotivoFalencia="
				+ protestoMotivoFalencia + ",\n instrumentoProtesto=" + instrumentoProtesto + ",\n vrDemaisDespesas="
				+ vrDemaisDespesas + ",\n complementoRegistro=" + complementoRegistro + ",\n nuSeqRegistroArquivo="
				+ nuSeqRegistroArquivo + "]";
	}

	
}
