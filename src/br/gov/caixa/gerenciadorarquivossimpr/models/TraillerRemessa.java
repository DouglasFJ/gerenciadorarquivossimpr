package br.gov.caixa.gerenciadorarquivossimpr.models;

public class TraillerRemessa {

    private String idRegistro;
    private String coPortador;
    private String noPortador;
    private String dtMovimento;
    private String somaSegurancaQtRemessa;
    private String somaSegurancaVrRemessa;
    private String compRegistro;
    private String nuSeqRegistroArq;
	public String getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(String idRegistro) {
		this.idRegistro = idRegistro;
	}
	public String getCoPortador() {
		return coPortador;
	}
	public void setCoPortador(String coPortador) {
		this.coPortador = coPortador;
	}
	public String getNoPortador() {
		return noPortador;
	}
	public void setNoPortador(String noPortador) {
		this.noPortador = noPortador;
	}
	public String getDtMovimento() {
		return dtMovimento;
	}
	public void setDtMovimento(String dtMovimento) {
		this.dtMovimento = dtMovimento;
	}
	public String getSomaSegurancaQtRemessa() {
		return somaSegurancaQtRemessa;
	}
	public void setSomaSegurancaQtRemessa(String somaSegurancaQtRemessa) {
		this.somaSegurancaQtRemessa = somaSegurancaQtRemessa;
	}
	public String getSomaSegurancaVrRemessa() {
		return somaSegurancaVrRemessa;
	}
	public void setSomaSegurancaVrRemessa(String somaSegurancaVrRemessa) {
		this.somaSegurancaVrRemessa = somaSegurancaVrRemessa;
	}
	public String getCompRegistro() {
		return compRegistro;
	}
	public void setCompRegistro(String compRegistro) {
		this.compRegistro = compRegistro;
	}
	public String getNuSeqRegistroArq() {
		return nuSeqRegistroArq;
	}
	public void setNuSeqRegistroArq(String nuSeqRegistroArq) {
		this.nuSeqRegistroArq = nuSeqRegistroArq;
	}
	@Override
	public String toString() {
		return "TraillerRemessa [idRegistro=" + idRegistro + ",\n coPortador=" + coPortador + ",\n noPortador=" + noPortador
				+ ",\n dtMovimento=" + dtMovimento + ",\n somaSegurancaQtRemessa=" + somaSegurancaQtRemessa
				+ ",\n somaSegurancaVrRemessa=" + somaSegurancaVrRemessa + ",\n compRegistro=" + compRegistro
				+ ",\n nuSeqRegistroArq=" + nuSeqRegistroArq + "]";
	}

    
}
