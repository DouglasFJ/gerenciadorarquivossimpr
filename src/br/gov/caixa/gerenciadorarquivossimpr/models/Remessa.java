package br.gov.caixa.gerenciadorarquivossimpr.models;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Remessa implements Cloneable {
	
	private HeaderRemessa header;
	
	private List<TransacaoRemessa> transacoes;
	
	private TraillerRemessa trailler;
	
	
	public void atualizarNumSeq() {
		
		DecimalFormat f4 = new DecimalFormat("0000");
		
		header.setNuSeqRegistroArq(f4.format(1));
		trailler.setNuSeqRegistroArq(f4.format(2+transacoes.size()));
		
		for(int i=0; i<transacoes.size(); i++) {
			transacoes.get(i).setNuSeqRegistroArquivo(f4.format(i+2));
		}
		
	}
	

	public HeaderRemessa getHeader() {
		return header;
	}

	public void setHeader(HeaderRemessa header) {
		this.header = header;
	}

	public List<TransacaoRemessa> getTransacoes() {
		return transacoes;
	}

	public void setTransacoes(List<TransacaoRemessa> transacoes) {
		List<TransacaoRemessa> trs = new ArrayList<>();
		trs.addAll(transacoes);
		this.transacoes = trs;
	}

	public TraillerRemessa getTrailler() {
		return trailler;
	}

	public void setTrailler(TraillerRemessa trailler) {
		this.trailler = trailler;
	}

	@Override
	public String toString() {
		return "Remessa [header=" + header + ", transacoes=" + transacoes + ", trailler=" + trailler + "]";
	}
	
	@Override
	public Remessa clone() throws CloneNotSupportedException {
		return (Remessa) super.clone();
	}
	
}
